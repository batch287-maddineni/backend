let someTrainer = {
	name : "Ash Ketchum",
	age : 10,
	pokemon : ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends : {
		hoenn : ["May", "Max"],
		kanto : ["Brock", "Misty"]
	},
	talk: function(){
		console.log('Pikachu! I choose you!');
	}

}
console.log(someTrainer);

console.log("Result of dot notation");
console.log(someTrainer.name);

console.log("Result of square bracket notation");
console.log(someTrainer["pokemon"]);

console.log("Result of talk method");
console.log(someTrainer.talk());

function Pokemon(name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name)
			target.health -= this.attack;
			console.log(target.name + "'s health is now reduced to " + target.health);
			if(target.health <= 0){
				target.faint();
			}
		};
		this.faint = function(){
			console.log(this.name + ' has fainted ');
		};
	};

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
	let geodude = new Pokemon('Geodude', 8);
	console.log(geodude);
	let mewtwo = new Pokemon("Mewtwo", 100);
	console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);