const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController")
const auth = require("../auth")

// router.post ("/",auth.verify,(req,res) =>{
//     const courseData =auth.decode(req.headers.authorization);
//     if (courseData.isAdmin){
//         courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
//     }else {
//         res.send({auth : "failed"})
//     }
    
// })

// module.exports = router;



router.post("/",auth.verify,(req,res)=>{

    const data = {
        course : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    };

    courseController.addCourse(data).then(resultFromController => res.send(
        resultFromController));
});
router.get("/all",(req,res)=>{

    courseController.getAllCourses().then(resultFromController => res.send(
        resultFromController));
});

router.get("/active-courses",(req,res)=>{

    courseController.activeCourses().then(resultFromController => res.send(
        resultFromController));
});
router.get("/:courseId",(req,res)=>{

    courseController.getCourse(req.params).then(resultFromController => res.send(
        resultFromController));
});
router.put("/:courseId",auth.verify,(req,res)=>{

    courseController.updateCourse(req.params,req.body).then(resultFromController => res.send(
        resultFromController));
});
router.patch("/:courseId",auth.verify,(req,res)=>{

    courseController.archiveCourse(req.params,req.body).then(resultFromController => res.send(
        resultFromController));
});


module.exports = router;