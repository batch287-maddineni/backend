// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

//[section] mongodb aggregation
  //used to generate manipulated data and perform operations to create filtered results that helps in analysing data

  //using the aggreagate method
    //the "$match" is used to pass documents that meet specified conditions to the next pipeline stage/aggregation processs.
    
    /*
        syntax:
            { $match: { field: value }}

        the "$group" is ysed to group elements together and field-values pairs using the data from the dgrouped elements.

        syntax:
            { $group: { _id: "value", fieldResult: "valueResult"}}
        using both "$match" and "$group" along with aggregation will find for the products that are onsale and will group the total amount of stocks for all the suppliers found.

        syntax:
             db.collectionName.aggregate([
              { $match: { fieldA: valueA }},
              { $group: {_id: "$fieldB"}, {result: {operation}}}
              ]);
    */

        //the "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
        //the "$sum" operator will total the values of all "stock" fie

  db.fruits.aggregate([
  { $match: { onSale: true }},
  { $group: { _id: "$supplier_id", total: {$sum: "$stock"}}}
  ]);


    db.fruits.aggregate([
      { $match: { onSale: true }},
      { $group: { _id: "$supplier_id", total: {$sum: "$stock"}}},
      {$project: {_id:0}}
      ]);


        db.fruits.aggregate([
      { $match: { onSale: true }},
      { $group: { _id: "$supplier_id", total: {$sum: "$stock"}}},
      {$sort: {total:-1}}
      ]);

        db.fruits.aggregate([
            {$unwind: "$origin"}
          ]);



    db.fruits.aggregate([
            { $unwind: "$origin" },
            { $group: { _id: "$origin" , fruits: {$push: "$name"}}}
        ]);