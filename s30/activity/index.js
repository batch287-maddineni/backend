  //P1
    db.fruits.aggregate([
  { $match: { onSale: true }},
  { $group: { _id: "$onSale", fruitsOnSale: {$sum: 1}}},
  { $project: {_id:0}}
  ]);

//P2
  db.fruits.aggregate([
  { $match: { stock: {$gte:20} }},
  { $group: { _id: "", enoughStock: {$sum: 1}}},
  { $project: {_id:0}}
  ]);

//P3
    db.fruits.aggregate([
  { $match: { onSale: true }},
  { $group: { _id: "$supplier_id", avg_price: {$avg: "$price"}}},
  ]);

//P4
    db.fruits.aggregate([
  { $match: { onSale: true }},
  { $group: { _id: "$supplier_id", max_price: {$max: "$price"}}}
  ]);

//P5
    db.fruits.aggregate([
  { $match: { onSale: true }},
  { $group: { _id: "$supplier_id", min_price: {$min: "$price"}}}
  ]);

