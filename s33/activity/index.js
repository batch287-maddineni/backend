	// let data = fetch("https://jsonplaceholder.typicode.com/posts/1").then((response) => response.json())
	// console.log(data);

	let titleArray = [];

	// for(let i = 1; i<=200; i++){
	// 	let data = fetch("https://jsonplaceholder.typicode.com/todos/1")
	// 				.then((response) => response.json())
	// 		titleArray += data.title;
	// }



	// async function fetchData(i){

	// 	let result = await fetch("https://jsonplaceholder.typicode.com/todos/"+ i);

	// 	let json = await result.json();

	// 	newTitle = json.title;
	// 	titleArray.length++;
	// 	titleArray[i-1] = newTitle;

	// };

	for(let i = 1; i<=200; i++){
	fetch("https://jsonplaceholder.typicode.com/todos/" + i)
	.then((response) => response.json())
	.then((json) => {titleArray[i-1] = json.title;})
	}

		fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response) => response.json())
	.then((json) => console.log(json))

		console.log(titleArray);


	fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then(response => response.json())
	.then(json => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));


	fetch("https://jsonplaceholder.typicode.com/todos", {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "New to do",
			completed: false,
			userId: 1,
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: "Updated to do",
			dateCompleted: "pending",
			status: "pending",
			description: "updating a list item to a different data structure",
			userId: 1,
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: "03/05/22",
			status: "completed",
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

		fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'DELETE'
	});