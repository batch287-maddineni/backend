//part1
db.users.find({ $or : [ { firstName: { $regex: 's', $options: 'i'}}, { lastName: { $regex: 'd', $options: 'i'}}]},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}).pretty();

//part2
db.users.find({ $and : [ { department: "HR" }, { age: {$gte: 70} }]}).pretty();

//part3
db.users.find({ $and : [ { firstName: { $regex: 'e', $options: 'i'}}, { age: {$lte: 30} }]}).pretty();
