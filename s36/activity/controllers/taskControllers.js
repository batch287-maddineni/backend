
const Task = require("../models/task")

module.exports.getAllTasks =() => {
    return Task.find({}).then(result =>{
        return result; 
    });
};

module.exports.getSpecificTasks = (taskId) => {
	return Task.findById(taskId).then((resultS, error) => {

		if(error){
			console.log(error);
			return false
		} else {
			return resultS
		};
	});
};

module.exports.updateTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err);
			return false;
		}

		result.status = "Complete";


		return result.save().then((updatedTask, saveErr) => {

			if(saveErr){

				console.log(saveErr); 
				return false;

			} else {

				return updatedTask;
			};
		});
	});
};


