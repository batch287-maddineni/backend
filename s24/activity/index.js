let getCube = 2 ** 3;

console.log(`The cube of 2 is ${getCube}`);

let address = ["258 Washington Ave NW", "California", "90011"];

let [street, city, zipCode] = address;

console.log(`I live at ${street}, ${city} ${zipCode}`);

let animal = {
	name : "Lolong",
	type : "saltwater crocodile",
	weight : "1075 kgs",
	lengthOf : "20 ft 3 in"
}

let {name, type, weight, length } = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement Of ${length}.`);

let numbers = [1,2,3,4,5];

numbers.forEach((num) => console.log(num));

let reduceNumber = numbers.reduce((x,y) => x+y);

console.log(reduceNumber);

class Dog {
		constructor(name, age, breed){
			this.age = age;
			this.name = name;
			this.breed = breed;
		}
	}

let myDog = new Dog("jackie", "3", "Dobberman");

console.log(myDog);